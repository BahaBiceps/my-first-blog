from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth.views import login
from blog.views import ProfilePage, RegisterView,logout_view
from blog.views import HomeView

urlpatterns = [
    url(r'^/$', HomeView.as_view),

    url(r'^admin/', admin.site.urls),
    url(r'', include('blog.urls')),

    url(r'^accounts/login/$', login, name="login"),
    url(r'^accounts/profile/$', ProfilePage.as_view(), name="profile"),

    url(r'^accounts/logout/$', logout_view, name="logout"),

    url(r'^accounts/register/$', RegisterView.as_view(), name="register"),

]
