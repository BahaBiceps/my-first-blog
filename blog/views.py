from django.contrib.auth import logout
from django.contrib.auth.models import User
from django.utils import timezone
from django.views.generic import TemplateView

from .models import Post
from django.shortcuts import redirect, render, get_object_or_404
from .forms import PostForm

class ProfilePage(TemplateView):
    template_name = "registration/profile.html"

def logout_view(request):
    logout(request)
    return redirect("/")

class RegisterView(TemplateView):
    template_name = "registration/register.html"

    def dispatch(self, request, *args, **kwargs):
        context = {}
        if request.method == 'POST':
            message = None
            username = request.POST.get("username")
            fullname = request.POST.get("fullname")
            email = request.POST.get("email")
            password = request.POST.get("password")
            password2 = request.POST.get("password2")
            if len(password) >= 8 and password == password2:
                user = User.objects.create_user(username, email, password)
                user.first_name = fullname
                user.save()
                message = "Регистрация успешна!"
            else:
                message = "Пароли не совпадают или пароль состоит больше 8 символов"
            context['message'] = message
        return render(request, self.template_name, context)

class HomeView(TemplateView):
    template_name = "blog/home.html"

def post_list(request):
    posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
    return render(request, 'blog/post_list.html', {'posts': posts})

def post_detail(request, pk):
    post = get_object_or_404(Post, pk=pk)
    return render(request, 'blog/post_detail.html', {'post': post})

def post_new(request):
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.published_date = timezone.now()
            post.save()
            return redirect('post_detail', pk=post.pk)
    else:
        form = PostForm()
    return render(request, 'blog/post_edit.html', {'form': form})
    
def delete_new(request,pk):
   u = New.objects.get(pk=pk).delete()
   if request.method == 'POST':
       form = DeleteNewForm(request.POST)    
       form.u.delete()             
       form.save()   
   return render_to_response('blog/post_edit.html', {
           'form': form,
           }, 
        instance=RequestContext(request)) 

def post_edit(request, pk):
    post = get_object_or_404(Post, pk=pk)
    if request.method == "POST":
        form = PostForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.published_date = timezone.now()
            post.save()
            return redirect('post_detail', pk=post.pk)
    else:
        form = PostForm(instance=post)
    return render(request, 'blog/post_edit.html', {'form': form})

